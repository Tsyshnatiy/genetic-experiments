// GeneticMaxCounter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "vector"
#include "Evolution.h"
#include "iostream"
#include "Constants.h"

using namespace std;

int main(void)
{
	int initCoeffs[CHROMO_CNT];

	for (int i = 0 ; i < CHROMO_CNT ; i++)
	{
		initCoeffs[i] = 1;
	}

	vector<int> coeffs(initCoeffs, initCoeffs + CHROMO_CNT);
	Evolution* ev = Evolution::Instance(1000, coeffs, 50, CHROMO_CNT);
	pair<bitset<CHROMO_CNT>, int> result = ev->GetSolution();

	if (result.first != NULL)
	{
		cout << "Solution was found during " << result.second << " iteration :"<< endl;
		for (__int8 i = 0 ; i < CHROMO_CNT ; i++)
		{
			cout << result.first.at(i) << " ";
		}

		cout << endl;
	}
	else
	{
		cout << "Solution was not found" << endl;
	}

	delete ev;
	return 0;
}

