#pragma once 
#ifndef EVOLUTION_H
#define EVOLUTION_H

#include "stdafx.h"
#include "bitset"
#include "memory"
#include "vector"
#include "Population.h"
#include "Constants.h"

using namespace std;

class Evolution
{
private:
	Population* pop;
    int evIterations;
    int expectedResult;
    static vector<int> coeffs;
	static Evolution* instance;

	explicit Evolution(const int, const vector<int>, const int, const int);

	struct Comparer
	{
	public:
		inline bool operator()(bitset<CHROMO_CNT> ind1, bitset<CHROMO_CNT> ind2)
		{
			int res1 = Evolution::Fitness(&ind1);
			int res2 = Evolution::Fitness(&ind2);

			return res1 > res2;
		}
	} comparer;

    static const int Fitness(const bitset<CHROMO_CNT>*);

	virtual const bitset<CHROMO_CNT> Select();
	void Mutate();
public:
	static Evolution* Instance(const int, const vector<int>, const int, const int);
	pair<bitset<CHROMO_CNT>, int> GetSolution();
	virtual ~Evolution();
};

#endif