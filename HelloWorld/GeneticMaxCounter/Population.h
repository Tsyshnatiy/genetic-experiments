#pragma once
#ifndef POPULATION_H
#define POPULATION_H

#include "stdafx.h"
#include "bitset"
#include "memory"
#include "time.h"
#include "vector"
#include "algorithm"
#include "Constants.h"

using namespace std;

class Population
{
private:
	int individsNumber;
	vector<bitset<CHROMO_CNT>> individs;

    void GenerateInitialPop();

	void Crossover(bitset<CHROMO_CNT>*, bitset<CHROMO_CNT>*, __int8);
public:
	Population(const int);

	virtual void GenerateNext();

	virtual void Mutate(bitset<CHROMO_CNT>*);

	inline const vector<bitset<CHROMO_CNT>>& GetIndivids();
	inline void SetIndivids(const vector<bitset<CHROMO_CNT>>&);

	int GetIndividsNumber() const;

	virtual ~Population();
};

#endif