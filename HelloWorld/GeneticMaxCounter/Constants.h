#pragma once 
#ifndef CONSTANTS_H
#define CONSTANTS_H

#define CHROMO_CNT 64

#define MUTATION_POSSIBILITY 0.5f / 100.0f

#define CROSSOVER_POSSIBILITY 1.0f / 100.0f

#endif