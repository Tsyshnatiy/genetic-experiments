#include "stdafx.h"
#include "Population.h"

#pragma warning( disable : 4244 )

void Population::GenerateInitialPop()
{
	for(int i = 0 ; i < individsNumber ; i++)
	{
		bitset<CHROMO_CNT> tmp;
		for (int j = 0 ; j < CHROMO_CNT ; j++)
		{
			bool dna = !!(rand() % 2);
			tmp.set(j, dna);
		}

		individs.push_back(tmp);
	}
}

void Population::Crossover(bitset<CHROMO_CNT>* parent1, bitset<CHROMO_CNT>* parent2, __int8 index = 3)
{
	if (rand() % 1000 < CROSSOVER_POSSIBILITY * 1000)
	{
		bitset<CHROMO_CNT> child;
		for (__int8 i = 0 ; i < CHROMO_CNT ; i++)
		{
			if ((*parent1)[i])
			{
				child.set(i, true);
			}
			else if((*parent2)[i])
			{
				child.set(i, true);
			}
		}

		individs.push_back(child);
	}
}

Population::Population(const int initIndividsNumber)
{
	individsNumber = initIndividsNumber;
	srand(time(NULL));
	GenerateInitialPop();
}

void Population::GenerateNext()
{
	vector<bitset<CHROMO_CNT>> individsCopy(individs);

	for (vector<bitset<CHROMO_CNT>>::size_type i = 0 ; i < individsCopy.size() - 1 ; i++)
	{
		Crossover(&individsCopy[i], &individsCopy[i + 1], rand() % CHROMO_CNT);
	}

	Crossover(&individsCopy[0], &individsCopy[individsCopy.size() - 1], rand() % CHROMO_CNT);
}

void Population::Mutate(bitset<CHROMO_CNT>* individ)
{
	__int8 index = rand() % CHROMO_CNT;
	individ->flip(index);
}

const vector<bitset<CHROMO_CNT>>& Population::GetIndivids() { return individs; }
void Population::SetIndivids(const vector<bitset<CHROMO_CNT>>& individs) { this->individs = individs; }

int Population::GetIndividsNumber() const { return individsNumber; }

Population::~Population()
{
}