#include "stdafx.h"
#include "Evolution.h"

vector<int> Evolution::coeffs(10);
Evolution* Evolution::instance = NULL;

Evolution* Evolution::Instance(const int initialIndivids, const vector<int> funcCoeffs, 
							   const int iterations, const int expResult)
{
	if (instance == NULL)
	{
		instance = new Evolution(initialIndivids, funcCoeffs, iterations, expResult);
	}

	return instance;
}

pair<bitset<CHROMO_CNT>, int> Evolution::GetSolution()
{
	int i = 0;

	while (i < evIterations)
	{
		pop->GenerateNext();
		Mutate();
		bitset<CHROMO_CNT> individ = Select();

		if (Fitness(&individ) == expectedResult)
		{
			return *(new pair<bitset<CHROMO_CNT>, int>(individ, i));
		}

		i++;
	}
	return *(new pair<bitset<CHROMO_CNT>, int>(NULL, evIterations));
}

const int Evolution::Fitness(const bitset<CHROMO_CNT>* ind)
{
	int result = 0;
	for (vector<int>::size_type i = 0 ; i < coeffs.size() ; i++)
	{
		result += coeffs[i] * ind->at(i);
	}

	return result;
}

const bitset<CHROMO_CNT> Evolution::Select()
{
	vector<bitset<CHROMO_CNT>> individs = pop->GetIndivids();
		
	sort(individs.begin(), individs.end(), comparer);
		
	vector<bitset<CHROMO_CNT>>::const_iterator first = individs.begin();
	vector<bitset<CHROMO_CNT>>::const_iterator last = individs.begin() + pop->GetIndividsNumber();
	vector<bitset<CHROMO_CNT>> newIndivids(first, last);

	pop->SetIndivids(newIndivids);

	return newIndivids[0];
}

void Evolution::Mutate()
{
	vector<bitset<CHROMO_CNT>> individs = pop->GetIndivids();

	for (vector<bitset<CHROMO_CNT>>::size_type i = 0 ; i < individs.size() ; i++)
	{
		bool isMutationNeeded = rand() % 1000 < MUTATION_POSSIBILITY * 1000.0f; 

		if (isMutationNeeded)
		{
			pop->Mutate(&individs[i]);
		}
	}
}

Evolution::Evolution(const int initialIndivids, const vector<int> funcCoeffs, 
					 const int iterations, const int expResult)
{
	Evolution::coeffs = funcCoeffs;
	evIterations = iterations;
	expectedResult = expResult;
	pop = new Population(initialIndivids);
}

Evolution::~Evolution()
{
	delete pop;
}
